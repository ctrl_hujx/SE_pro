package com.pink.se.viewobject;

import java.util.Date;

/**
 * @Author James17
 * @Date 2020/2/10 12:45
 *
 * 报告详情
 **/
public class ReportDetailVO {

    private Integer id;

    private String title;

    private String category;  //关联


    private String commitStaff;


    private String receiveStaff;


    private Date commitTime;

    private Integer status;

    //version
    private Integer version;

    //最新提交时间
    private Date createAt;

    //报告内容
    private String reportContent;

    //提交说明
    private String message;





}
