package com.pink.se.viewobject;

import com.pink.se.model.Report;
import com.pink.se.model.Version;

import java.util.Date;

/**
 * @Author James17
 * @Date 2020/2/10 12:25
 **/
public class ReportListVO  {

    private Integer id;

    private String title;

    private String category;  //关联


    private String commitStaff;


    private String receiveStaff;

//首次提交时间
    private Date commitTime;

    private String commitTimeSTD;

//最新提交时间
    private Date createAt;

    private String lastTimeSTD;

    private Integer status;


    public String getCommitTimeSTD() {
        return commitTimeSTD;
    }

    public void setCommitTimeSTD(String commitTimeSTD) {
        this.commitTimeSTD = commitTimeSTD;
    }

    public String getLastTimeSTD() {
        return lastTimeSTD;
    }

    public void setLastTimeSTD(String lastTimeSTD) {
        this.lastTimeSTD = lastTimeSTD;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCommitStaff() {
        return commitStaff;
    }

    public void setCommitStaff(String commitStaff) {
        this.commitStaff = commitStaff;
    }

    public String getReceiveStaff() {
        return receiveStaff;
    }

    public void setReceiveStaff(String receiveStaff) {
        this.receiveStaff = receiveStaff;
    }

    public Date getCommitTime() {
        return commitTime;
    }

    public void setCommitTime(Date commitTime) {
        this.commitTime = commitTime;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
