package com.pink.se.viewobject;

import javax.validation.constraints.NotNull;

/**
 * @Author James17
 * @Date 2020/2/4 10:31
 *
 * 草稿箱报告对象
 * 主要用于修改草稿箱内的报告对象
 **/
public class DraftReportDTO {

    //报告本身信息
    public String title;

    public Integer categoryId;

    private Integer receiveStaffId;//接收者id

    //报告属性
    private Integer id;//主键  ,首次提交自动生成

    //版本控制属性
    @NotNull
    private String reportContent;//报告版本内容

    @NotNull
    private String message;//提交说明


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getReceiveStaffId() {
        return receiveStaffId;
    }

    public void setReceiveStaffId(Integer receiveStaffId) {
        this.receiveStaffId = receiveStaffId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getReportContent() {
        return reportContent;
    }

    public void setReportContent(String reportContent) {
        this.reportContent = reportContent;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
