package com.pink.se.viewobject;

/**
 * @Author James17
 * @Date 2020/2/11 10:03
 *
 *回复传输接收对象
 **/
public class ReplyDTO {
    private String content;

    private Integer reportId;


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getReportId() {
        return reportId;
    }

    public void setReportId(Integer reportId) {
        this.reportId = reportId;
    }
}
