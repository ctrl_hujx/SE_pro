package com.pink.se.service;

import com.pink.se.model.Position;

import java.util.List;

public interface PositionService {
    List<Position> getAllPosition();

}
