package com.pink.se.service.Impl;

import com.pink.se.exception.DIYException;
import com.pink.se.mapper.ReplyMapper;
import com.pink.se.mapper.ReportReplyMapper;
import com.pink.se.model.Reply;
import com.pink.se.model.ReportReply;
import com.pink.se.model.Staff;
import com.pink.se.service.ReplyService;
import com.pink.se.viewobject.ReplyDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @Author James17
 * @Date 2020/2/11 10:08
 **/

@Service
public class ReplyServiceImpl implements ReplyService {

@Autowired
    ReplyMapper replyMapper;
@Autowired
    ReportReplyMapper reportReplyMapper;

    @Override
    public boolean reply(ReplyDTO replyDTO, Staff staffInfo) {
        if (!validString(replyDTO.getContent())){//验证不通过
            throw new DIYException("回复内容不能为空！");
        }
        //回复操作
        Reply reply = new Reply();
        reply.setReplyStaffId(staffInfo.getId());
        reply.setContent(replyDTO.getContent());
        reply.setReplyTime(new Date());
        replyMapper.insertSelective(reply);

        ReportReply reportReply = new ReportReply();
        reportReply.setReportId(replyDTO.getReportId());
        reportReply.setReplyId(reply.getId());

        return reportReplyMapper.insertSelective(reportReply)>0;
    }

    private boolean validString(String content) {
        if (null == content || "".equals(content))
        return false;
        else return true;
    }
}
