package com.pink.se.service.Impl;

import com.pink.se.exception.DIYException;
import com.pink.se.mapper.StaffMapper;
import com.pink.se.model.Staff;
import com.pink.se.service.StaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author James17
 * @Date 2020/2/10 11:00
 **/
@Service
public class StaffServiceImpl implements StaffService {

    @Autowired
    StaffMapper staffMapper;


    @Override
    public Staff login(Staff staff) {
       Staff dbStaff =  staffMapper.selectStaffByEmail(staff.getEmail());
       if (dbStaff==null){
           throw new DIYException("账号或者密码错误");
       }
       if (dbStaff.getPassword().equals(staff.getPassword())){
           //登录成功
           dbStaff.setPassword(null);
           return dbStaff;
       }
        return null;
    }
}
