package com.pink.se.service.Impl;

import com.pink.se.exception.DIYException;
import com.pink.se.mapper.CollectionMapper;
import com.pink.se.mapper.ReportMapper;
import com.pink.se.mapper.VersionMapper;
import com.pink.se.model.Collection;
import com.pink.se.model.Report;
import com.pink.se.model.Version;
import com.pink.se.viewobject.DraftReportDTO;
import com.pink.se.viewobject.FirstReportDTO;
import com.pink.se.service.ReportService;
import com.pink.se.viewobject.RepeatReportDTO;
import com.pink.se.viewobject.ReportListVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class ReportServiceImpl   implements ReportService {

    @Autowired
    ReportMapper reportMapper;

    @Autowired
    VersionMapper versionMapper;

    @Autowired
    CollectionMapper collectionMapper;







/**
 *@Author James17
 *@Date 2020/2/11 11:24
 * 审批报告
**/

    @Override
    public boolean approval(Integer reportId, Integer key) {
        if (key !=1 && key != 2)
            throw new DIYException("审批结果error");//

        Report report = validateReport(reportId);
        if (key==1){//审批通过
            report.setStatus(-1);
        }
        if (key==2){//审批不过
            report.setStatus(-2);
        }
        return reportMapper.updateByPrimaryKeySelective(report)>0;
    }


/**
 *@Author James17
 *@Date 2020/2/10 11:58
 * 查询报告
**/

    @Override
    public List<ReportListVO> queryReportList(Integer id, Integer posId) {
        //根据权限查询
        List <ReportListVO> list = null;
        if (posId==2)  //领导
            list = reportMapper.queryReportListLeader(id);

        if (posId==1)//员工
            list =  reportMapper.queryReportListStaff(id);

        if (list!=null) {
            List<ReportListVO> listVOS = dateTransfer(list);
            return listVOS;
        }


        return null;
    }



    private List<ReportListVO> dateTransfer(List<ReportListVO> list) {
        for (ReportListVO r:list) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd:HH-mm-ss");
            r.setCommitTimeSTD(sdf.format(r.getCommitTime()));
            r.setLastTimeSTD(sdf.format(r.getCreateAt()));
            r.setCommitTime(null);
            r.setCreateAt(null);
        }
        return list;
    }

    /**
     *@Author James17
     *@Date 2020/2/6 9:54
     *@return boolean
    **/
    @Override
    public boolean uncollectReport(Integer reportId,Integer staffId) {
        //报告号校验
        Report report = validateReport(reportId);
        //是否被收藏
        Collection collection = collectionMapper.isCollectedByThisStaff(reportId,staffId);
        if (collection==null)//未被收藏
            throw new DIYException("该报告暂未被收藏");
        //已被收藏,取消收藏即可
            return  collectionMapper.deleteByPrimaryKey(collection.getId())>0;
    }



    /**
     *@Author James17
     *@Date 2020/2/6 9:47
     *@return boolean
     * 收藏报告
    **/

    @Override
    public boolean collectReport(Integer reportId,Integer staffId) {
        //报告号校验
        Report report = validateReport(reportId);
        if (report.getDeleteStatus()!=0)
            throw new DIYException("仅有未被删除的报告才能收藏，请将报告还原后重试");

        //判断报告是否被该员工收藏
        if (collectionMapper.isCollectedByThisStaff(reportId,staffId)!=null) //已被收藏
            throw new DIYException("该报告已被该职工收藏");
        //收藏报告
        Collection collection = new Collection();
        collection.setCollecterId(staffId);
        collection.setReportId(reportId);

        return collectionMapper.insertSelective(collection)>0;
    }



    /**
     *@Author James17
     *@Date 2020/2/6 9:40
     *@return boolean
     * 员工恢复软删除报告
    **/
    @Override
    public boolean stuffRestore(Integer reportId) {
        Report report = validateReport(reportId);
        report.setDeleteStatus((byte)0);
        return reportMapper.updateByPrimaryKeySelective(report)>0;
    }



    /**
     *@Author James17
     *@Date 2020/2/6 9:32
     *@return boolean
     * 员工彻底删除报告
    **/
    @Override
    public boolean stuffDeleteReally(Integer reportId) {
        Report report = validateReport(reportId);
        report.setDeleteStatus((byte)-1);

        return reportMapper.updateByPrimaryKeySelective(report)>0;
    }


    /**
     *@Author James17
     *@Date 2020/2/6 9:24
     *@return boolean
     * 员工软删除报告
    **/

    @Override
    public boolean stuffDeleteSoft(Integer reportId) {
        Report report = validateReport(reportId);
        report.setDeleteStatus((byte)1);
        return reportMapper.updateByPrimaryKeySelective(report)>0;
    }


    private Report validateReport(Integer reportId) {
        Report report = reportMapper.selectByPrimaryKey(reportId);
        if (report == null){
            //恢复软删除报告失败
            throw new DIYException("数据库无该报告信息，请确认后重试");
        }
        return report;
    }


    /**
 *@Author James17
 *@Date 2020/2/4 11:06
 *@return boolean
 * 提交草稿箱内的报告
**/

    @Override
    public boolean commitDraft(DraftReportDTO draftReportDTO) {
        //获取reportId  得到report
        Report dbReport = reportMapper.selectByPrimaryKey(draftReportDTO.getId());
        if (dbReport.getStatus() != 0) {
            //throw new
            System.out.println("非草稿箱内报告!!!");
            return false;
        }
        dbReport.setStatus(1);
        Version version = versionMapper.selectByPrimaryKey(dbReport.getVersionId());
        version.setCreatedAt(new Date());
        return  reportMapper.updateByPrimaryKeySelective(dbReport)>0&&versionMapper.updateByPrimaryKeySelective(version)>0;
    }



    /**
     *@Author James17
     *@Date 2020/2/4 10:36
     *@return boolean
     * 修改草稿箱内报告的内容
    **/

    @Override
    public boolean updateDraftReport(DraftReportDTO draftReportDTO) {
        //获取reportId  更新report
        Report dbReport = reportMapper.selectByPrimaryKey(draftReportDTO.getId());
        BeanUtils.copyProperties(draftReportDTO,dbReport);
        if (dbReport.getStatus()!=0){
            //throw
            System.out.println("非草稿箱报告!!!");
        }
        //更新version信息
        Version version = versionConvertFromDraftReport(dbReport, draftReportDTO);
        int t1= versionMapper.insertSelective(version);
        dbReport.setVersionId(version.getId());
        int t2 = reportMapper.updateByPrimaryKeySelective(dbReport);
        return t1>0&&t2>0;
    }




    /**
     *@Author hjx
     *@Description
     * 首次提交报告或者存入草稿箱
     * 将报告存入表中，并进行版本控制
     *@Date 2019/11/21 11:53    2020/02/04修改
     *@return boolean
    **/
    @Transactional
    @Override
    public boolean commitReport(FirstReportDTO firstReportDTO) {
        //1 分离Version对象
        Version version = versionConvertFromFirstReportVO(firstReportDTO);
        int temp1   =  versionMapper.insertSelective(version);

        //2 分离Report对象
        Report report =reportConvertFromFirstReportVO(firstReportDTO,version);
        int temp2=0;

        if (report.getStatus()==1||report.getStatus()==0&&report.getId()==null){
                //此时将report插入表中
                temp2=reportMapper.insertSelective(report);
                //将reportId插入到version中
                version.setReportId(report.getId());
                versionMapper.updateByPrimaryKeySelective(version);
        }
        return temp2>0&&temp1>0;
    }

    /**
     *@Author hjx
     *@Description
     * 重新提交报告
     * 记录新版本，更新现有报告
     *@Date 2019/11/21 11:51
     *@Param [repeatReportDTO]
     *@return boolean
    **/
    @Override
    @Transactional
    public boolean repeatCommitReport(RepeatReportDTO repeatReportDTO) {
        //1 分离Version对象,记录版本
        Version version = versionConvertFromRepeatReportVO(repeatReportDTO);
        int temp1   =  versionMapper.insertSelective(version);
        //2 分离Report对象，更新Report对象
        Report report =reportConvertFromRepeatReportVO(repeatReportDTO,version);
        int temp2=0;
        if (report.getStatus()>1){
            //此时只需要修改report对象
            temp2= reportMapper.updateByPrimaryKeySelective(report);
        }
        return temp2>0&&temp1>0;
    }




    /**
     *@Author hjx
     *@Description
     * 通过报告id获取当前报告的最大版本号
     *@Date 2019/11/21 12:20
     *@Param [reportId]
     *@return int
    **/
    @Override
    public int getCurrentReportMaxVersionByReportId(Integer reportId) {
        return versionMapper.getCurrentReportMaxVersionByReportId(reportId);
    }

    /**
     *@Author hjx
     *@Description
     *转换方法，将前端传来的首次报告对象从包装类分离出来
     *@Date 2019/11/21 12:21
     *@Param [firstReportDTO]
     *@return com.pink.se.model.Version
    **/

    private Version versionConvertFromFirstReportVO(FirstReportDTO firstReportDTO) {
        Version version = new Version();
        BeanUtils.copyProperties(firstReportDTO,version);
        //还需要处理的属性   reportId; reportVersion; createdAt; replyIds;
        //对reportId的处理，如果是首次提交是没有报告id的，得首先生成报告id。在commitReport中会有处理
        //首次提交reportVersion默认为1
        version.setReportVersion(1);
        //设置版本提交时间
        version.setCreatedAt(new Date());

        //返回插入版本对象的结果(int)
        return version;

    }
    /**
     *@Author hjx
     *@Description
     * 转换方法，将前端传来的非首次提交报告对象从包装类分离出来
     *@Date 2019/11/21 12:24
     *@Param [repeatReportDTO]
     *@return com.pink.se.model.Version
    **/

    private Version versionConvertFromRepeatReportVO(RepeatReportDTO repeatReportDTO){
        //对非首次提交的记录进行处理
        Version version = new Version();
        BeanUtils.copyProperties(repeatReportDTO,version);
        //将版本自己的id设为null，清空BeanUtils的赋值
        version.setId(null);
        //设置版本的报告id
        version.setReportId(repeatReportDTO.getId());
        //对应版本的记录号+1
        version.setReportVersion(getCurrentReportMaxVersionByReportId(version.getReportId())+1);
        //设置当前版本的提交时间
        version.setCreatedAt(new Date());

        //返回插入版本对象的结果(int)
        return  version;
    }

    private Version versionConvertFromDraftReport(Report dbReport, DraftReportDTO draftReportDTO) {
        Version version = new Version();
        BeanUtils.copyProperties(draftReportDTO,version);
        version.setId(null);
        version.setReportId(dbReport.getId());
        version.setReportVersion(getCurrentReportMaxVersionByReportId(version.getReportId())+1);
        version.setCreatedAt(new Date());
        return version;

    }



    private Report reportConvertFromFirstReportVO(FirstReportDTO firstReportDTO, Version dbVersion) {
        Report report = new Report();
        BeanUtils.copyProperties(firstReportDTO,report);
        // 需要处理的属性 versionId; commitTime;  status;  deleteStatus;
        //逻辑判断   根据reportVo.getId()判断是否第一次提交，
        if (firstReportDTO.getDraftBox()==1){
            //存入草稿箱
            System.out.println("存入草稿箱");
            report.setCommitTime(new Date());
            report.setVersionId(dbVersion.getId());
            report.setStatus(0);
            return report;
        }
         else if(report.getId()==null){
            //report.id为null则是首次提交
            System.out.println("report首次提交");
            report.setStatus(1);
            //首次提交时间
            report.setCommitTime(new Date());
            report.setVersionId(dbVersion.getId());
            report.setDeleteStatus((byte)0);
            return report;
        }
     return null;
    }

    private Report reportConvertFromRepeatReportVO(RepeatReportDTO repeatReportDTO, Version dbVersion){
        Report report = new Report();
        BeanUtils.copyProperties(repeatReportDTO,report);

        if(repeatReportDTO.getId()!=null){    //视为已经提交过
            //获取数据库中完整的报告对象
            Report dbReport = reportMapper.selectByPrimaryKey(repeatReportDTO.getId());
            //设置报告对应的版本的id号
            report.setVersionId(dbVersion.getId());
            //将报告的status加1
            report.setStatus(dbReport.getStatus()+1);
            return report;
        }
        return null;
    }
}
