package com.pink.se.service;

import com.pink.se.model.Staff;
import com.pink.se.viewobject.ReplyDTO;

/**
 * @Author James17
 * @Date 2020/2/11 10:07
 **/
public interface ReplyService {
    boolean reply(ReplyDTO replyDTO, Staff staffInfo);
}
