package com.pink.se.service;

import com.pink.se.model.Category;

public interface CategoryService {
    boolean addCategory(Category category);
}
