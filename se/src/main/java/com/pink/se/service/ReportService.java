package com.pink.se.service;

import com.pink.se.model.Report;
import com.pink.se.viewobject.DraftReportDTO;
import com.pink.se.viewobject.FirstReportDTO;
import com.pink.se.viewobject.RepeatReportDTO;
import com.pink.se.viewobject.ReportListVO;

import java.util.List;

public interface ReportService {
    boolean commitReport(FirstReportDTO firstReportDTO);

    int getCurrentReportMaxVersionByReportId(Integer reportId);

    boolean repeatCommitReport(RepeatReportDTO repeatReportDTO);

    boolean updateDraftReport(DraftReportDTO draftReportDTO);

    boolean commitDraft(DraftReportDTO draftReportDTO);

    boolean stuffDeleteSoft(Integer reportId);

    boolean stuffDeleteReally(Integer reportId);

    boolean stuffRestore(Integer reportId);

    boolean collectReport(Integer reportId,Integer staffId);

    boolean uncollectReport(Integer reportId,Integer staffId);

    List<ReportListVO> queryReportList(Integer id, Integer posId);

    boolean approval(Integer reportId, Integer key);
}
