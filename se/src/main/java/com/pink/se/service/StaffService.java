package com.pink.se.service;

import com.pink.se.model.Staff;

public interface StaffService {

    Staff login(Staff staff);
}
