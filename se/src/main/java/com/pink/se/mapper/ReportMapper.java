package com.pink.se.mapper;

import com.pink.se.model.Report;
import com.pink.se.viewobject.ReportListVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ReportMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Report record);

    int insertSelective(Report record);

    Report selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Report record);

    int updateByPrimaryKey(Report record);


    List<ReportListVO> queryReportListLeader(@Param("receiveStaffId") Integer receiveStaffId);

    List<ReportListVO> queryReportListStaff(@Param("commitStaffId")Integer commitStaffId);
}