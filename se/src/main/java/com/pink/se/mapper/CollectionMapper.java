package com.pink.se.mapper;

import com.pink.se.model.Collection;
import org.apache.ibatis.annotations.Param;

public interface CollectionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Collection record);

    int insertSelective(Collection record);

    Collection selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Collection record);

    int updateByPrimaryKey(Collection record);

    Collection isCollectedByThisStaff(@Param("reportId") Integer reportId, @Param("staffId")Integer staffId);

}