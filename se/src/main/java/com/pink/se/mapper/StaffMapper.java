package com.pink.se.mapper;

import com.pink.se.model.Staff;
import org.apache.ibatis.annotations.Param;

public interface StaffMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Staff record);

    int insertSelective(Staff record);

    Staff selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Staff record);

    int updateByPrimaryKey(Staff record);

    Staff selectStaffByEmail(@Param("email") String email);
}