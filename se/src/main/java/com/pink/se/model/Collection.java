package com.pink.se.model;

public class Collection {
    private Integer id;

    private Integer collecterId;

    private Integer reportId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCollecterId() {
        return collecterId;
    }

    public void setCollecterId(Integer collecterId) {
        this.collecterId = collecterId;
    }

    public Integer getReportId() {
        return reportId;
    }

    public void setReportId(Integer reportId) {
        this.reportId = reportId;
    }
}