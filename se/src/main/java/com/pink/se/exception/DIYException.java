package com.pink.se.exception;

/**
 * @Author James17
 * @Date 2020/2/6 9:25
 **/
public class DIYException extends  RuntimeException {
    private String msg;

    public DIYException() {
        super();
    }
    public DIYException(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
