package com.pink.se.controller;


import com.pink.se.exception.DIYException;
import com.pink.se.model.Report;
import com.pink.se.model.Staff;
import com.pink.se.response.RespBean;
import com.pink.se.service.ReportService;
import com.pink.se.viewobject.DraftReportDTO;
import com.pink.se.viewobject.FirstReportDTO;
import com.pink.se.viewobject.RepeatReportDTO;
import com.pink.se.viewobject.ReportListVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/report")
public class ReportController {
    @Autowired
    ReportService reportService;

    @Autowired
    HttpServletRequest request;


    /**
     *@Author James17
     *@Date 2020/2/11 9:59
     * 获取用户信息
    **/

    private Staff getStaffInfo() {
        Object obj=request.getSession().getAttribute("StaffInfo");
        if (obj == null)
            throw new DIYException("请登陆后尝试");
        else return (Staff) obj;
    }




    /**
     *@Author James17
     *@Date 2020/2/11 11:20
     * 领导对报告的审批（决定是否通过）
     * key:是否通过  1通过 ，2不通过
    **/
    @PostMapping("/approval/{reportId}/{key}")
    public RespBean approval(@PathVariable("reportId")Integer reportId,@PathVariable("key") Integer key){
        if(reportService.approval(reportId,key)){
            return RespBean.success("审批成功");
        }
        return RespBean.fail("审批失败");
    }


    /**
     *@Author James17
     *@Date 2020/2/10 11:50
     * 查看报告
    **/
    @GetMapping("/list")
    public RespBean reportList(){
        Staff staffInfo = getStaffInfo();
        List<ReportListVO> list=reportService.queryReportList(staffInfo.getId(),staffInfo.getPosId());
        if (list!=null){
            return RespBean.success("查询成功",list);
        }
        return RespBean.fail("查询失败");
    }

    /**
     *@Author James17
     *@Date 2020/2/6 9:53
     *@return
     * 取消报告收藏
    **/
    @PostMapping("/uncollect/{reportId}")
    public RespBean uncollectReport(@PathVariable("reportId") Integer reportId){
        Staff staffInfo =getStaffInfo();
        if (reportService.uncollectReport(reportId,staffInfo.getId())){
            return  RespBean.success("取消收藏报告成功");
        }
        return RespBean.fail("取消收藏报告失败");
    }


    /**
     *@Author James17
     *@Date 2020/2/6 9:44
     *@return
     * 收藏报告
    **/
    @PostMapping("/collect/{reportId}")
    public RespBean collectReport(@PathVariable("reportId") Integer reportId){
        Staff staffInfo =getStaffInfo();
        if (reportService.collectReport(reportId,staffInfo.getId())){
            return  RespBean.success("收藏报告成功");
        }
        return RespBean.fail("收藏报告失败");
    }




    /**
     *@Author James17
     *@Date 2020/2/6 9:37
     *@return
     * 员工恢复软删除报告
    **/
    @PostMapping("delete/restore/{reportId}")
    public RespBean stuffRestore(@PathVariable("reportId") Integer reportId){
        if (reportService.stuffRestore(reportId)){
            return  RespBean.success("还原报告成功");
        }
        return RespBean.fail("还原报告失败");
    }



    /**
     *@Author James17
     *@Date 2020/2/6 9:28
     *@return
     * 员工彻底删除报告
    **/
    @PostMapping("delete/really/{reportId}")
    public RespBean stuffDeleteReally(@PathVariable("reportId") Integer reportId){
        if (reportService.stuffDeleteReally(reportId)){
            return  RespBean.success("彻底删除成功");
        }
        return RespBean.fail("彻底删除失败");
    }


    /**
     *@Author James17
     *@Date 2020/2/6 9:19
     *@return
     * 员工软删除报告
    **/
    @PostMapping("delete/soft/{reportId}")
    public RespBean stuffDeleteSoft(@PathVariable("reportId") Integer reportId){
        if (reportService.stuffDeleteSoft(reportId)){
            return  RespBean.success("删除成功");
        }
        return RespBean.fail("删除失败");
    }


    /**
     *@Author James17
     *@Date 2020/2/4 11:04
     * 将草稿箱报告提交
     **/
    @PostMapping("/commit/draft")
    public RespBean commitDraft(@RequestBody DraftReportDTO draftReportDTO){
        if (reportService.commitDraft(draftReportDTO)){
            return  RespBean.success("提交草稿新报告成功");
        }
        return RespBean.fail("提交草稿新报告失败");
    }

    /**
     *@Author James17
     *@Date 2020/2/4 10:12
     *@return
     * 对草稿箱内报告的修改
    **/
    @PutMapping("/update/draft")
    public RespBean updateDraftReport(@RequestBody @Valid DraftReportDTO draftReportDTO){
        if (reportService.updateDraftReport(draftReportDTO)){
           return  RespBean.success("更新草稿新报告成功");
        }
        return RespBean.fail("更新草稿新报告失败");
    }


   /**
    *@Author hjx
    *@Description 首次提交报告
    *@Date 2019/11/21 11:35
   **/
    @PostMapping("/first")
    public RespBean firstCommitReport(@RequestBody  @Valid FirstReportDTO firstReportDTO){
        //将报告包装对象分离出来，分表存储。
        if (firstReportDTO !=null){
            if (firstReportDTO.getDraftBox()==1){    //存入草稿箱
                if (reportService.commitReport(firstReportDTO)){
                    return  RespBean.success("存入草稿箱成功");
                }
                return  RespBean.fail("存入草稿箱失败");
            }
            else if(reportService.commitReport(firstReportDTO)) { //首次提交
                return  RespBean.success("提交成功");
            }

        }
        return RespBean.fail("提交失败");
    }




/**
 *@Author James17
 *@Date 2020/2/4 10:10
 * 持续交互下提交报告
**/
    @PutMapping("/repeat")
    public RespBean repeatCommitReport(@RequestBody  @Valid RepeatReportDTO repeatReportDTO){
        //将报告包装对象分离出来，分表存储。
        if (repeatReportDTO !=null){
            if (reportService.repeatCommitReport(repeatReportDTO)) {
                return  RespBean.success("repeat提交成功");
            }
        }
        return RespBean.fail("repeat提交失败");
    }





}
