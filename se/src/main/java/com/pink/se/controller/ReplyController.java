package com.pink.se.controller;

import com.pink.se.exception.DIYException;
import com.pink.se.model.Staff;
import com.pink.se.response.RespBean;
import com.pink.se.service.ReplyService;
import com.pink.se.viewobject.ReplyDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author James17
 * @Date 2020/2/11 9:58
 **/
@RestController
@RequestMapping("/reply")
public class ReplyController {

    @Autowired
    HttpServletRequest request;
    @Autowired
    ReplyService replyService;



    /**
     *@Author James17
     *@Date 2020/2/11 9:59
     * 获取用户信息
     **/

    private Staff getStaffInfo() {
        Object obj=request.getSession().getAttribute("StaffInfo");
        if (obj == null)
            throw new DIYException("请登陆后尝试");
        else return (Staff) obj;
    }

    //需要content和report的id
    @PostMapping("/")
    public RespBean reply(@RequestBody ReplyDTO replyDTO){
        Staff staffInfo = getStaffInfo();
        if (replyService.reply(replyDTO,staffInfo)){
            return RespBean.success("回复成功");
        }
        return RespBean.fail("回复失败！");
    }
}
