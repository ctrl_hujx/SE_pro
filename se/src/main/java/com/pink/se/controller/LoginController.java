package com.pink.se.controller;

import com.pink.se.exception.DIYException;
import com.pink.se.model.Staff;
import com.pink.se.response.RespBean;
import com.pink.se.service.StaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author James17
 * @Date 2020/2/10 10:53
 **/
@RestController
@RequestMapping("/login")
public class LoginController {

    @Autowired
    StaffService staffService;

    @Autowired
    HttpServletRequest request;
    @PostMapping("/")
    public RespBean login(@RequestBody Staff staff){
        if (staff==null || staff.getEmail()==null || staff.getPassword()==null){
            throw new DIYException("登录信息错误");
        }

        Staff validStaff = staffService.login(staff);
        if (validStaff!=null){//登录成功
           request.getSession().setAttribute("StaffInfo",validStaff);
           return RespBean.success("登录成功",validStaff);
        }
        return RespBean.fail("账号或密码错误，请重新登录");
    }
}
