//教师姓名
export const validName =(str)=>{
	str=valueTrim(str)
	if(str==null || str==''){
			layer.msg('姓名不能为空')
			return false;
		}
	 if(str.length<2){
		 layer.msg('姓名必须大于一位')
		 return false;
	 }
		return true;
}

// 班级名称
export const validClassName =(str)=>{
	if(str==null || str==''){
			layer.msg('班级名不能为空')
			return false;
		}
	 if(str.length<3){
		 layer.msg('班级名必须大于三位')
		 return false;
	 }
		return true;
}

// 班级编号
export const validClassId = (id)=>{
	if(id==null||id ==''){
		layer.msg('班级id为空')
		return false;
	}
	if(!isNumber(id)){
		layer.msg('班级id必须为正整数')
		return false;
	}
	if(id<0){
		Toast.fail('班级id必须>0')
		return false;
	}
	if(id>10000){
		layer.msg('班级id必须<10000')
		return false;
	}
	return true;
	
}

export const isNumber=(numberValue)=>{
 	//定义正则表达式部分    
 	var reg1 = /^[0-9]{0,}$/;
 	var reg2 = /^[1-9]{1}[0-9]{0,}$/;
 	//alert(numberValue);
 	if(numberValue ==null || numberValue.length==0){
 		return false;
 	}
 	numberValue = valueTrim(numberValue);
 	//判断当数字只有1位时
 	if(numberValue.length<2){
 		return reg1.test(numberValue);
 	}
 	return reg2.test(numberValue);
}

// const dropBlank=function(str){	
// 	 var s = str.replace(/ /g,'')	
// 	console.log( s)
// 	 return  s; 
// }

const  valueTrim=function(str) {   
    return str.replace(/(^\s*)|(\s*$)/g, "");   
}