// var scriptFile = document.createElement('script');
// scriptFile.setAttribute("type","text/javascript");
// scriptFile.setAttribute("src",'https://unpkg.com/axios/dist/axios.min.js');
// document.getElementsByTagName("head")[0].appendChild(scriptFile);
 
 
 // axios.defaults.withCredentials = true
 axios.interceptors.response.use(success=>{
 	if (success.status && success.status==200 && success.data.status==500) {	 
 		return success.data;
 	} 
 	if (success.data.msg) {
 			// layer.msg({message:success.data.msg})
 	}
 	return success.data;
 	
 	},error=>{
 	 if(error.response.status==504||error.response.status==404){
 		 	// layer.msg({message:'服务器被吃了( ╯□╰ )'});
 	 }else if(error.response.status==403){
 		 // layer.msg({message:'权限不足，请联系管理员'});		 
 	 }else if(error.response.status==401){
 		 // layer.msg({message:'尚未登录，请登录'});		 
 	 }else{
 		 if(error.response.data.msg){
 			
 			// layer.msg({message:error.response.data.msg});
 			 
 		 }else{
 			 // layer.msg({message:'未知错误!'});
 		 }
 	 }
 	 return ;
 })
 
 
 
 
	
 let base ='http://localhost:1999';

export const postKeyValueRequest=(url,params)=>{
	return axios({
		method:'post',
		url:`${base}${url}`,
		data:params,
		transformRequest:[function(data){
			let ret ='';
			for(let i in data){
				
				ret+=encodeURIComponent(i)+'='+encodeURIComponent(data[i])+'&'
			}
			return ret;
		}],
		headers:{
			'Content-Type':'application/x-www-form-urlencoded'
		} 
		 
		
	});
}

export const postRequest=(url,params)=>{
	return axios({
		method:'post',
		url:`${base}${url}`,
		data:params
	})
}

export const putRequest=(url,params)=>{
	return axios({
		method:'put',
		url:`${base}${url}`,
		data:params
	})
}

export const getRequest=(url,params)=>{
	return axios({
		method:'get',
		url:`${base}${url}`,
		data:params
	})
}

export const deleteRequest=(url,params)=>{
	return axios({
		method:'delete',
		url:`${base}${url}`,
		data:params
	})
}